/*
 *  spi.h
 *
 *  Created on: 2019-08-15
 *  Author: rafau
 */

#ifndef _RU_STM8LIB_SPI_H_
#define _RU_STM8LIB_SPI_H_

#include <stdint.h>

/**
 * Initalize spi
 * spi_clock = freq/(2^(clk_div+1)) // 0 <= clk_div <= 7
 */
void spi_init(uint8_t clk_div, uint8_t cpol, uint8_t phase);

/**
 * Put byte to spi
 */
uint8_t spi_transfer(uint8_t byte);

/**
 * Get byte from spi
 */
uint8_t spi_get(void);

#endif /* !_RU_STM8LIB_SPI_H_ */
