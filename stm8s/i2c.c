/*
 *  i2c.c
 *
 *  Created on: 2019-11-11
 *  Author: rafau
 */

#include "i2c.h"

#include <stdio.h>
#include <stdlib.h>

#include "clock.h"
#include "stm8s.h"

static i2c_freq_t _i2c_freq;
static uint8_t _i2c_first;

static void _i2c_delay(uint16_t anz) {
    volatile uint16_t count;
    uint8_t foo = 0;

    for (count = 0; count < anz; count++) {
        // __asm
        //   nop
        //   nop
        // __endasm;
        foo = count;
    }
}

uint8_t i2c_init(i2c_freq_t freq) {
    if (freq > i2c_freq_SIZE) return 1;

    I2C_CR1 = 0x00;
    // I2C_FREQR = sysclock_freq() / 1000000;
    I2C_FREQR = 16;

    _i2c_freq = freq;

    // only for f_cpu 16 Mhz
    switch (freq) {
        case i2c_freq_100KHz:
            I2C_CCRL = 0x50;
            I2C_CCRH &= ~(I2C_CCRH_FS | I2C_CCRH_DUTY | I2C_CCRH_CCR_8_11_MASK);
            break;
        case i2c_freq_50KHz:
            I2C_CCRL = 0xA0;
            I2C_CCRH &= ~(I2C_CCRH_FS | I2C_CCRH_DUTY | I2C_CCRH_CCR_8_11_MASK);
            break;
        case i2c_freq_15KHz:
            I2C_CCRL = 0x23;
            I2C_CCRH &= ~(I2C_CCRH_FS | I2C_CCRH_DUTY | I2C_CCRH_CCR_8_11_MASK);
            I2C_CCRH |= I2C_CCRH_FS;
            break;

        default: break;
    }

    // I2C_CR1 |= I2C_CR1_ENGC;  // General call enabled. Address 0x00 is ACKed.
    I2C_CR2 &= ~I2C_CR2_POS;
    I2C_CR2 &= ~I2C_CR2_ACK;

    I2C_TRISER = 0x11;
    //  I2C_CR2 |= I2C_CR2_POS;
    I2C_OARH = I2C_OARH_ADDCONF;  // This bit must set by software (must always be written as 1).

    I2C_CR1 |= I2C_CR1_PE;  // Peripheral enable
    return 0;
}

uint8_t i2c_busy(void) {
    return (I2C_SR3 & I2C_SR3_BUSY);
}

void i2c_start(void) {
    // char dummy;

    // while (i2c_busy()) {}

    // I2C_CR2 |= I2C_CR2_ACK;
    I2C_CR2 |= I2C_CR2_START;

    while (!(I2C_SR1 & I2C_SR1_SB)) {}  // wait for start condition generated

    // dummy = I2C_SR1;
    // _i2c_first = 1;
}

void i2c_stop(void) {
    I2C_CR2 |= I2C_CR2_STOP;
    while (I2C_SR3 & I2C_SR3_MSL) {}  // wait to stop condition on the bus
}

uint8_t i2c_write(uint8_t value) {
    uint8_t ret;
    volatile uint8_t dummy;
    uint8_t timcx, timcmp;
    timcmp = 13;

    I2C_DR = value;
    timcx = 0;
    while (((I2C_SR1 & I2C_SR1_ADDR) == 0) && (timcx < timcmp)) {
        _i2c_delay(5);
        timcx++;
    }
    dummy = I2C_SR3;

    if (I2C_SR2 & I2C_SR2_AF)
        ret = 0;
    else
        ret = 1;
    I2C_SR2 &= ~(I2C_SR2_AF);

    return ret;
}

uint8_t i2c_read(void) {
    static uint8_t value;
    static uint8_t dummy;

    I2C_CR2 &= ~(I2C_CR2_POS);

    if (!(_i2c_first)) {
        I2C_CR2 |= I2C_CR2_ACK;
        value = I2C_DR;
        I2C_SR1 &= ~(I2C_SR1_RXNE);
    }

    I2C_CR2 &= ~(I2C_CR2_POS);
    I2C_CR2 |= I2C_CR2_ACK;
    value = I2C_DR;

    _i2c_first = 0;
    return value;
}