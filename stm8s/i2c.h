/*
 *  i2c.h
 *
 *  Created on: 2019-11-11
 *  Author: rafau
 */

#ifndef _RU_STM8LIB_I2C_H_
#define _RU_STM8LIB_I2C_H_

#include <stdint.h>

typedef enum i2c_freq_select {
    i2c_freq_100KHz = 0,  //
    i2c_freq_50KHz,
    i2c_freq_15KHz,
    i2c_freq_SIZE
} i2c_freq_t;

uint8_t i2c_init(i2c_freq_t freq);
uint8_t i2c_busy(void);
void i2c_start(void);
void i2c_stop(void);
uint8_t i2c_write(uint8_t value);
uint8_t i2c_read(void);

#endif /* !_RU_STM8LIB_I2C_H_ */
