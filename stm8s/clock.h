/*
 *  clock.h
 *
 *  Created on: 2019-08-15
 *  Author: rafau
 */

#ifndef _RU_STM8LIB_CLOCK_H_
#define _RU_STM8LIB_CLOCK_H_

#include <stdint.h>

/**
 * Initalize clock
 * xtal_enbl = 1 -> use external xtal
 * xtal_enbl = 0 -> use internal oscilator (sets HSI divider to 1)
 */
void sysclock_init(uint8_t xtal_enbl);

/**
 * Returns master clock frequency if known
 */
int32_t sysclock_freq();

/**
 * Disable clock generating for all peripherals
 */
void sysclock_disable_all_peripherals(void);

void sysclock_enable_tim1(void);
void sysclock_enable_tim3(void);
void sysclock_enable_tim2_tim5(void);
void sysclock_enable_tim4_tim6(void);
void sysclock_enable_usart(void);
void sysclock_enable_spi(void);
void sysclock_enable_i2c(void);
void sysclock_enable_can(void);
void sysclock_enable_adc(void);
void sysclock_enable_awu(void);

/**
 * Blocking delay
 */
int8_t blocking_delay_ms(uint32_t ms);

#endif /* !_RU_STM8LIB_CLOCK_H_ */
