/*
 *  clock.c
 *
 *  Created on: 2019-08-15
 *  Author: rafau
 */

#include "clock.h"

#include <stdio.h>
#include <stdlib.h>

#include "stm8s.h"

void sysclock_init(uint8_t xtal_enbl) {
    if (xtal_enbl) {
        CLK_ICKR = 0;
        CLK_ECKR = CLK_ECKR_HSEEN;

        while (!(CLK_ECKR & CLK_ECKR_HSERDY)) {}

        CLK_SWR = CLK_SWR_HSE;
        CLK_SWCR = CLK_SWCR_SWEN;

        CLK_CKDIVR = 0;
        CLK_PCKENR1 = 0xFF;
        CLK_PCKENR2 = 0xFF;
    } else {
        CLK_ICKR = 0;
        CLK_ECKR = 0;

        CLK_ICKR = CLK_ICKR_HSIEN;
        while (!(CLK_ICKR & CLK_ICKR_HSIRDY)) {}

        CLK_CKDIVR = 0;
        CLK_PCKENR1 = 0xFF;
        CLK_PCKENR2 = 0xFF;

        CLK_CCOR = 0;
        CLK_HSITRIMR = 0;
        CLK_SWIMCCR = 0;
        CLK_SWR = CLK_SWR_HSI;
        CLK_SWCR = 0;
        CLK_SWCR = CLK_SWCR_SWEN;
        while ((CLK_SWCR & CLK_SWCR_SWBSY)) {}
    }
}

int32_t sysclock_freq() {
    int32_t freq = (int32_t)F_CPU;
    uint8_t div;
    switch (CLK_SWR) {
        case CLK_SWR_HSE: return freq;
        case CLK_SWR_HSI:
            div = CLK_CKDIVR & (0x03 << 3);
            if (div == CLK_CLKDIVR_HSIDIV_div1) return freq;
            if (div == CLK_CLKDIVR_HSIDIV_div2) return freq / 2;
            if (div == CLK_CLKDIVR_HSIDIV_div4) return freq / 4;
            if (div == CLK_CLKDIVR_HSIDIV_div8) return freq / 8;
        case CLK_SWR_LSI: return 128000;

        default: return -1;
    }
    return -1;
}

void sysclock_disable_all_peripherals(void) {
    CLK_PCKENR1 = 0x00;
    CLK_PCKENR2 &= ~(CLK_PCKENR2_PCKEN2_7 | CLK_PCKENR2_PCKEN2_3 | CLK_PCKENR2_PCKEN2_2);
}

void sysclock_enable_tim1(void) {
    CLK_PCKENR1 |= CLK_PCKENR1_PCKEN1_7;
}

void sysclock_enable_tim3(void) {
    CLK_PCKENR1 |= CLK_PCKENR1_PCKEN1_6;
}

void sysclock_enable_tim2_tim5(void) {
    CLK_PCKENR1 |= CLK_PCKENR1_PCKEN1_5;
}

void sysclock_enable_tim4_tim6(void) {
    CLK_PCKENR1 |= CLK_PCKENR1_PCKEN1_4;
}

void sysclock_enable_usart(void) {
    CLK_PCKENR1 |= CLK_PCKENR1_PCKEN1_3;
    CLK_PCKENR1 |= CLK_PCKENR1_PCKEN1_2;
}

void sysclock_enable_spi(void) {
    CLK_PCKENR1 |= CLK_PCKENR1_PCKEN1_1;
}

void sysclock_enable_i2c(void) {
    CLK_PCKENR1 |= CLK_PCKENR1_PCKEN1_0;
}

void sysclock_enable_can(void) {
    CLK_PCKENR2 |= CLK_PCKENR2_PCKEN2_7;
}

void sysclock_enable_adc(void) {
    CLK_PCKENR2 |= CLK_PCKENR2_PCKEN2_3;
}

void sysclock_enable_awu(void) {
    CLK_PCKENR2 |= CLK_PCKENR2_PCKEN2_2;
}

int8_t blocking_delay_ms(uint32_t ms) {
    int32_t freq = sysclock_freq();
    if (freq < 0) return -1;

    uint32_t i;
    uint32_t l = ((freq / 1000UL) / 18) * ms;
    ;

    for (i = 0; i < l; i++) { __asm__("nop"); }

    return 0;
}