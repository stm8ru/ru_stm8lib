/*
 *  usart.c
 *
 *  Created on: 2019-08-15
 *  Author: rafau
 */

#include "usart.h"

#include <stdio.h>
#include <stdlib.h>

#include "clock.h"
#include "stm8s.h"

uint8_t usart_init(uint32_t baud) {
    USART1_CR2 |= USART_CR2_TEN | USART_CR2_REN;
    USART1_CR3 &= ~(USART_CR3_STOP1 | USART_CR3_STOP2);

    int32_t clk = sysclock_freq();
    if (clk < 0) return 1;

    uint16_t dvsr = clk / baud;
    USART1_BRR1 = (dvsr >> 4) & 0xFF;
    USART1_BRR2 = ((dvsr & 0xF000) >> 8) | (dvsr & 0x0F);

    return 0;
}

void usart_putc(char c) {
    while (!(USART1_SR & USART_SR_TXE)) {}
    USART1_DR = c;
}

char usart_getc(void) {
    while (!(USART1_SR & USART_SR_RXNE)) {}
    return USART1_DR;
}

void usart_puts(char *s) {
    while (*s) usart_putc(*s++);
}