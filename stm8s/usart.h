/*
 *  usart.h
 *
 *  Created on: 2019-08-15
 *  Author: rafau
 */

#ifndef _RU_STM8LIB_USART_H_
#define _RU_STM8LIB_USART_H_

#include <stdint.h>

/**
 * Initialize uart, no parity, 8 data bits, 1 stop bit
 */
uint8_t usart_init(uint32_t baud);

/**
 * Blocking, send byte to uart
 */
void usart_putc(char c);

/**
 * Blocking, receive byte from uart
 */
char usart_getc(void);

/**
 * Blocking, send string to uart
 */
void usart_puts(char *s);

#endif /* !_RU_STM8LIB_USART_H_ */
