/*
 *  spi.c
 *
 *  Created on: 2019-08-15
 *  Author: rafau
 */

#include "spi.h"

#include "clock.h"
#include "stm8s.h"

void spi_init(uint8_t clk_div, uint8_t cpol, uint8_t phase) {
    if (cpol)
        SPI_CR1 |= SPI_CR1_CPOL;
    else
        SPI_CR1 &= ~SPI_CR1_CPOL;

    if (phase)
        SPI_CR1 |= SPI_CR1_CPHA;
    else
        SPI_CR1 &= ~SPI_CR1_CPHA;

    SPI_CR1 |= SPI_CR1_MSTR | SPI_CR1_SPE;
    SPI_CR1 &= ~(SPI_CR1_BR0 | SPI_CR1_BR1 | SPI_CR1_BR2);
    clk_div &= 0x07;
    SPI_CR1 |= (clk_div << 3);
}

uint8_t spi_transfer(uint8_t byte) {
    while (SPI_SR & SPI_SR_BSY) {}
    while (!(SPI_SR & SPI_SR_TXE)) {}
    SPI_DR = byte;
    return SPI_DR;
}

uint8_t spi_get(void) {
    return SPI_DR;
}