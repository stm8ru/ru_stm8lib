# ru_stm8lib

## How to add to project

Adding submodule to project:
```sh
mkdir -p libs && cd libs
git submodule add https://gitlab.com/stm8ru/ru_stm8lib.git
# or
git submodule add git@gitlab.com:stm8ru/ru_stm8lib.git 
```
and add modules to your makefile
```Makefile
INCDIRS += ./libs/
SRC += libs/ru_stm8lib/stm8s/usart.c  # example
```

